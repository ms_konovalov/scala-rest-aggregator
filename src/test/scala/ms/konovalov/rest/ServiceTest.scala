package ms.konovalov.rest

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import com.typesafe.config.ConfigFactory
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationDouble
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

class ServiceTest(_system: ActorSystem) extends TestKit(_system) with WordSpecLike with Matchers with BeforeAndAfterAll {

  def this() = this(ActorSystem("MySpec", ConfigFactory.parseString(
    """akka {
    |  loggers = ["akka.event.slf4j.Slf4jLogger"]
    |  loglevel = "DEBUG"
    |  logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"
    }""".stripMargin)))

  implicit val materializer = ActorMaterializer()

  "All futures should be completed" in {
    val f1 = future("1", 100, Array(DataItem("1", "1", 100, 12.9)))
    val f2 = future("2", 1000, Array(DataItem("2", "2", 400, 152.9)))
    val f3 = future("3", 3000, Array(DataItem("3", "3", 500, 452.9)))
    val futures = Seq(f1, f2, f3)
    val response = Await.result(Service.extractDataFromFutures(futures), 4 seconds)
    response.responseTimes.size shouldEqual 3
    response.items.size shouldEqual 3
  }

  "Exceeding timeout futures should be dropped" in {
    val f1 = future("1", 100, Array(DataItem("1", "1", 100, 12.9)))
    val f2 = future("2", 1000, Array(DataItem("2", "2", 400, 152.9)))
    val f3 = future("3", 3000, Array(DataItem("3", "3", 500, 452.9)))
    val futures = Seq(f1, f2, f3)
    val (fast, all) = Service.extractDataFromFuturesWithTimeout(2 seconds, futures)
    val fastResponse = Await.result(fast, 3 seconds)
    fastResponse.responseTimes.size shouldEqual 2
    fastResponse.items.size shouldEqual 2
    val allResponse = Await.result(all, 4 seconds)
    allResponse.responseTimes.size shouldEqual 3
    allResponse.items.size shouldEqual 3
  }

  private def future(id: String, timeout: Long, items: Array[DataItem]) = {
    Future {
      Thread.sleep(timeout)
      ApiCallResult(data = Right(items), serviceId = id, executionTime = timeout)
    }
  }

  override def afterAll {
    TestKit.shutdownActorSystem(system, 20 seconds)
  }
}
