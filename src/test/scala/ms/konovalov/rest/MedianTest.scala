package ms.konovalov.rest

import org.scalatest.{FreeSpec, Matchers}
import org.scalatest.prop.TableDrivenPropertyChecks

class MedianTest extends FreeSpec with Matchers with TableDrivenPropertyChecks {

  val args = Table(
    ( "input",
      "expected"),
    ( Seq(),
      Seq()),
    ( Seq(5),
      Seq(5)),
    ( Seq(1, 2),
      Seq(1)),
    ( Seq(1, 5, 10),
      Seq(1, 5)),
    ( Seq(1, 1, 10, 1, 1, 1, 1),
      Seq(1, 1, 1, 1, 1, 1)),
    ( Seq(10, 10, 10, 1, 10, 10, 10),
      Seq(1, 10, 10, 10, 10, 10, 10)),
    ( Seq(1, 2, 2, 2, 2, 10, 10),
      Seq(1, 2, 2, 2, 2))
  )

  "Median should be calculated and exceeding elements dropped" in {
    forAll(args) { (input, expected) =>
      val result = Median.findAndDropAbove[Int](input, _ > _)
      result shouldBe expected
    }
  }
}
