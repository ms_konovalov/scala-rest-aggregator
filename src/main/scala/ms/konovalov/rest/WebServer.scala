package ms.konovalov.rest

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory

import scala.concurrent.duration.DurationDouble
import scala.io.StdIn
import scala.language.postfixOps

/**
  * Main class to run
  */
object WebServer extends App {

  implicit val system = ActorSystem("my-system", ConfigFactory.load())
  implicit val materializer = ActorMaterializer()
  implicit val ec = system.dispatcher
  val service: ExternalServiceProcessor = Service

  val host = if (args.length > 0 && args(0) != null) args(0) else "localhost"
  val port = if (args.length > 1 && args(1) != null) args(1).toInt else 8080

  import akkahttptwirl.TwirlSupport._

  val route = get {
    pathSingleSlash {
      logRequest("root page") {
        complete {
          html.main.render()
        }
      }
    } ~
      path("search") {
        logRequest("request") {
          onComplete(service.requestData) { result =>
            complete {
              result.map(html.search.render)
            }
          }
        }
      } ~
      path("quicksearch") {
        logRequest("request") {
          onComplete({
            val (fast, all) = service.requestDataWithTimeLimit(2 seconds)
            all.map(logResponse)
            fast
          }) { result =>
            complete {
              result.map(html.search.render)
            }
          }
        }
      }
  }

  val bindingFuture = Http().bindAndHandle(route, host, port)

  println(s"Server started on interface $host and port $port")
  println("Press any key to exit...")

  StdIn.readLine()

  bindingFuture.flatMap(_.unbind()).onComplete(_ => system.terminate())

  private def logResponse(response: Response): Unit = {
    system.log.info("printing full result")
    response.responseTimes.foreach {case (service: String, time: Long) => system.log.info(s"service $service took $time")}
    response.items.foreach(item => system.log.info(s"$item"))
  }
}

