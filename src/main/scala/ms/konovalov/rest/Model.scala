package ms.konovalov.rest

import akka.http.scaladsl.model._
import ms.konovalov.rest.Model.HttpResult
import spray.json.{DefaultJsonProtocol, RootJsonFormat}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport

object Model {

  /**
    * Type alias for external service call result
    *
    * @tparam T type of success response
    */
  type HttpResult[T] = Either[ApiError, T]
}

/**
  * Container for external service call result
  *
  * @param data [[Either]] of [[Array]] or [[ApiError]]
  * @param serviceId service identifier
  * @param executionTime time spent on execution
  */
case class ApiCallResult(data: HttpResult[Array[DataItem]], serviceId: String, executionTime: Long)

/**
  * Trait representing external service call error
  */
sealed trait ApiError

/**
  * Class for external service call with non-OK status code
  * @param status http status code
  */
case class UnexpectedStatusCode(status: StatusCode) extends ApiError

/**
  * Class representing piece of data from external service
  * equals and hashCode are overridden to provide possibility to remove duplicates using distinct method
  *
  * @param id identifier
  * @param service service identifier
  * @param rank item's rank
  * @param price item's price
  */
case class DataItem(id: String, service: String, rank: Int, price: BigDecimal) {

  override def canEqual(a: Any): Boolean = a.isInstanceOf[DataItem]

  override def equals(that: Any): Boolean =
    that match {
      case that: DataItem => that.canEqual(this) && this.hashCode == that.hashCode
      case _ => false
    }

  override def hashCode: Int = {
    val prime = 31
    var result = 1
    result = prime * result + rank
    result = prime * result + price.hashCode
    result
  }
}

/**
  * Deserialization data from json
  */
trait ApiSerialization extends SprayJsonSupport with DefaultJsonProtocol {

  implicit val modelFormat: RootJsonFormat[DataItem] = jsonFormat4(DataItem)
}
