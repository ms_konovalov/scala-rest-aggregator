package ms.konovalov.rest

import scala.annotation.tailrec

/**
  * Median computation
  */
object Median {

  /**
    * Function calculates median and drops items that exceeds it
    * Recursive, worst case O(n2), avg case 0(n)
    *
    * @param items set of elements
    * @param comparator comparator
    * @tparam T type of elements
    * @return set of elements that are less or equal to median value
    */
  def findAndDropAbove[T](items: Seq[T], comparator: (T, T) => Boolean): Seq[T] =
    if (items.isEmpty) Seq.empty else findAndDropAboveInt(items, comparator, items.size - 1 >> 1, Seq.empty, choosePivotRandom)

  @tailrec
  private def findAndDropAboveInt[T](items: Seq[T], comparator: (T, T) => Boolean, position: Int, acc: Seq[T], choosePivot: Seq[T] => T): Seq[T] = {
    val pivot = choosePivot(items)
    val (abovePivot, rest) = items.partition(comparator(_, pivot))
    val (underPivot, equalPivot) = rest.partition(comparator(pivot, _))
    if (underPivot.size > position) {
      findAndDropAboveInt(underPivot, comparator, position, acc, choosePivot)
    } else if (underPivot.size + equalPivot.size > position) {
      acc ++ underPivot ++ equalPivot
    } else {
      findAndDropAboveInt(abovePivot, comparator, position - underPivot.size - equalPivot.size, acc ++ underPivot ++ equalPivot, choosePivot)
    }
  }

  private def choosePivotRandom[T]: Seq[T] => T = items => items(scala.util.Random.nextInt(items.size))

  private def choosePivot0[T]: Seq[T] => T = items => items.head

}
