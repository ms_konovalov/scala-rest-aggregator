package ms.konovalov.rest

import akka.actor.ActorSystem
import akka.stream.Materializer
import akka.stream.scaladsl.{Flow, Keep, Sink, Source}

import scala.concurrent.duration.FiniteDuration
import scala.concurrent.{ExecutionContext, Future}

/**
  * Container for response to client request
  *
  * @param responseTimes [[Map]] of service identifier and time spent on corresponding service call
  * @param items list of joined data from all services
  */
case class Response(responseTimes: Map[String, Long], items: Seq[DataItem]) {

  def add(serviceId: String, executionTime: Long, additional: Seq[DataItem]): Response = {
    Response(responseTimes + (serviceId -> executionTime), items ++ additional)
  }
}

/**
  * Helper class
  */
object Response {

  def apply(): Response = Response(Map.empty, Seq.empty)
}

/**
  * Trait for business logic
  */
trait ExternalServiceProcessor {

  /**
    * Execute calls to all services and wait until all finished
    *
    * @param mat materializer
    * @param ec execution context
    * @param system actor system
    * @return collected data as [[Response]]
    */
  def requestData(implicit mat: Materializer, ec: ExecutionContext, system: ActorSystem): Future[Response]

  /**
    * Execute calls to all services until timeout occurred. All data from services that exceeds timeout will be dropped
    * Also all items with price above median price will be dropped. Items ordered from lowest rank, price
    *
    * @param timeout timeout value
    * @param mat materializer
    * @param ec execution context
    * @param system actor system
    * @return tuple of 2 [[Response]]s: 1st - data received within timeout, 2nd - all data received
    */
  def requestDataWithTimeLimit(timeout: FiniteDuration)(implicit mat: Materializer, ec: ExecutionContext, system: ActorSystem): (Future[Response], Future[Response])
}

/**
  * Implementation of business logic
  */
object Service extends ExternalServiceProcessor {

  private val services: List[String] = List("mars", "jupiter", "saturnus")

  private def url(serviceId: String): String = s"https://6p9tp6qjdf.execute-api.eu-west-1.amazonaws.com/latest/res?service=$serviceId"

  override def requestData(implicit mat: Materializer, ec: ExecutionContext, system: ActorSystem): Future[Response] = {
    val futures = services.map(s => RestClient.getData(url(s), s))
    extractDataFromFutures(futures).map(resp =>
      Response(resp.responseTimes, filterAboveMedian(resp.items.distinct)))
  }

  private[rest] def extractDataFromFutures(futures: Seq[Future[ApiCallResult]])
                                          (implicit mat: Materializer, ec: ExecutionContext, system: ActorSystem): Future[Response] = {
    for {
      list <- Future.sequence(futures)
    } yield {
      val result = list.foldLeft(Response())(foldResponse)
      result.responseTimes.foreach { case (s, l) => system.log.debug(s"service $s call took $l ms") }
      result.items.foreach(item => system.log.debug(item.toString))
      result
    }
  }

  override def requestDataWithTimeLimit(timeout: FiniteDuration)(implicit mat: Materializer, ec: ExecutionContext, system: ActorSystem): (Future[Response], Future[Response]) = {
    val futures = services.map(s => RestClient.getData(url(s), s))
    val (fastResult, allResult) = extractDataFromFuturesWithTimeout(timeout, futures)
    (
      fastResult.map((resp) => Response(resp.responseTimes, filterAboveMedian(resp.items.distinct))),
      allResult.map((resp) => Response(resp.responseTimes, filterAboveMedian(resp.items.distinct)))
    )
  }

  private[rest] def extractDataFromFuturesWithTimeout(timeout: FiniteDuration, futures: Seq[Future[ApiCallResult]])
                                                     (implicit mat: Materializer, ec: ExecutionContext, system: ActorSystem): (Future[Response], Future[Response]) = {
    val source = futures.map(Source.fromFuture).foldLeft(Source.empty[ApiCallResult])((acc, b) => acc.merge(b))
    val fold = Sink.fold[Response, ApiCallResult](Response())(foldResponse)
    val toClient = Flow[ApiCallResult].takeWithin(timeout).toMat(fold)(Keep.right)
    val continue = Flow[ApiCallResult].toMat(fold)(Keep.right)

    source.alsoToMat(toClient)(Keep.right).toMat(continue)(Keep.both).run()
  }

  private def foldResponse: (Response, ApiCallResult) => Response =
    (acc: Response, b: ApiCallResult) => acc.add(b.serviceId, b.executionTime, b.data.getOrElse(Array[DataItem]()).toSeq)

  private def filterAboveMedian(result: Seq[DataItem]): Seq[DataItem] = {
    Median.findAndDropAbove[DataItem](result, _.price > _.price).sortWith(order)
  }

  private def order(a1: DataItem, a2: DataItem): Boolean =
    if (a1.rank == a2.rank) a1.price < a2.price else a1.rank < a2.rank
}
