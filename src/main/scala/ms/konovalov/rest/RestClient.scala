package ms.konovalov.rest

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.Uri.Path
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.{Unmarshal, Unmarshaller}
import akka.stream.Materializer
import akka.stream.scaladsl.{Sink, Source}
import ms.konovalov.rest.Model.HttpResult

import scala.concurrent.{ExecutionContext, Future}

/**
  * Rest client that provides http call
  */
object RestClient extends ApiSerialization {

  /**
    * Make call with [[Http#singleRequest]]
    *
    * @param url       service url
    * @param serviceId service identifier
    * @param mat       materializer
    * @param ec        execution context
    * @param system    actor system
    * @return future of [[ApiCallResult]] as a result of execution
    */
  def getData(url: String, serviceId: String)(implicit mat: Materializer, ec: ExecutionContext, system: ActorSystem): Future[ApiCallResult] = {
    val start = System.currentTimeMillis
    system.log.debug(s"call to $serviceId started")
    Http().singleRequest(HttpRequest(uri = url)).flatMap { response =>
      val time = System.currentTimeMillis - start
      system.log.debug(s"call to $serviceId finished for $time")
      deserialize[Array[DataItem]](response).map(data =>
        ApiCallResult(data = data, serviceId = serviceId, executionTime = time))
    }
  }

  /**
    * Make call with akka-streams approach
    *
    * @param host      service host
    * @param path      url path
    * @param serviceId service identifier
    * @param mat       materializer
    * @param ec        execution context
    * @param system    actor system
    * @return future of [[ApiCallResult]] as a result of execution
    */
  def getDataStream(host: String, path: String, serviceId: String)(implicit mat: Materializer, ec: ExecutionContext, system: ActorSystem): Future[ApiCallResult] = {
    val source = Source.single(HttpRequest(uri = Uri(path = Path(path))))
    val start = System.currentTimeMillis
    system.log.info(s"call to $serviceId started")
    val flow = Http().outgoingConnectionHttps(host).mapAsync(1) { response =>
      val time = System.currentTimeMillis - start
      system.log.info(s"call to $serviceId finished for $time")
      deserialize[Array[DataItem]](response).map(data =>
        ApiCallResult(data = data, serviceId = serviceId, executionTime = time))
    }
    source.via(flow).runWith(Sink.head)
  }

  private def deserialize[T](r: HttpResponse)(implicit um: Unmarshaller[ResponseEntity, T], mat: Materializer, ec: ExecutionContext): Future[HttpResult[T]] =
    r.status match {
      case StatusCodes.OK => Unmarshal(r.entity).to[T] map Right.apply
      case _ => Future(Left(UnexpectedStatusCode(r.status)))
    }
}
