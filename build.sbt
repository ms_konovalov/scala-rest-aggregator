resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"

resolvers += "Bartek's repo at Bintray" at "https://dl.bintray.com/btomala/maven"

lazy val root = (project in file("."))
  .settings(
    name := """rest-aggregator""",
    version := "1.0",
    libraryDependencies ++= Dependencies.main,
    scalaVersion := "2.12.2",
    parallelExecution in Test := false
  ).enablePlugins(SbtTwirl)

mainClass in (Compile, run) := Some("ms.konovalov.rest.WebServer")




