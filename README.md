REST aggregator service
=======================

This service provides the following functionality: it requests data from external REST service in internet for 3 different providers that respod with different (quite big) response time and combine all the data into one list.

It supports 2 **modes**: 

1. Fully synchronous (wait for all services to respond)
2. Respond within timeout (2 seconds), if service didn't respond within timeout it's data will not be sent to client, but processed in background (in this particular case just printed into console log)

**Data analysis**:
data, received from all services combined into one list, duplicates (similat Rank and Price) removed, also items with Price more than median are dropped

### How to run
To compile and start application go to root directory and execute
 
    % sbt package
    [info] Compiling 3 Scala sources to ./rest-aggregator/target/scala-2.12/classes...
    [info] Packaging ./rest-aggregator/target/scala-2.12/rest-aggregator_2.12-1.0.jar ...
    [info] Done packaging.
    [success] Total time: 11 s, completed Apr 27, 2017 11:32:21 AM
    
    % sbt run 127.0.0.1 8080
    [info] Running ms.konovalov.rest.WebServer 127.0.0.1 8989
    Server started on interface 127.0.0.1 and port 8989
    Press any key to exit...
    
### User interface
Open your browser and enter host and port provided in run command (e.g. http://localhost:8080).
On main page you will see 2 links: **"Search"** for fully synchronous mode and **"Fast search"** for search within 2 sec timeout.
After clicking on one of them you will see web page with table of results.

**Warning**: request may take some time to execute, so be patient!

If you choose option "Fast search" and all services respond more than 2 sec you will see "All services exceeded timeout"
Then take a look at console log to see full data.

### Used technologies
This service is built with akka-http, akka-streams and twirl.